const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const autoprefix = require('gulp-autoprefixer');
const babel = require('gulp-babel');

const basePath = {
    src: './src/',
    dist: './dist/'
};

const src = {
    css: basePath.src + 'css/',
    js: basePath.src + 'js/',
    img: basePath.src + 'img/'
};
const dist = {
    css: basePath.dist + 'css/',
    js: basePath.dist + 'js/',
    img: basePath.dist + 'img/'
};

gulp.task('css-min', function () {
   return gulp.src(src.css + '/**/*.css')
       .pipe(sourcemaps.init())
       .pipe(autoprefix('last 2 version'))
       .pipe(cleanCSS())
       .pipe(rename({suffix: '.min'}))
       .pipe(concat('style.min.css'))
       .pipe(sourcemaps.write('./'))
       .pipe(gulp.dest(dist.css))
});

gulp.task('compress', function () {
    return gulp.src(src.js + '/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(dist.js))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dist.js));
});

gulp.task('images', function () {
   return gulp.src(src.img + '/**/*.*')
       .pipe(imagemin({optimizationLevel: 7, progressive: true}))
       .pipe(gulp.dest(dist.img));
});

gulp.task('watch', function () {
   gulp.watch(src.css + '/**/*.css', ['css-min']);
   gulp.watch(src.js + '*.js', ['compress']);
   gulp.watch(src.img + '/**/*.*', ['images']);
});

gulp.task('default', ['css-min', 'compress', 'images', 'watch']);