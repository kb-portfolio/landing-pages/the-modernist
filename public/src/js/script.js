$(function () {

    (getAjaxLatestWork(6, 0));

    //main slider
    $('.slider').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnHover: true,
    });

    $('.navigation .hamburger').on("click", function () {
        $('.navigation .hamburger').toggleClass("is-active")
        $('.navigation .side-drawer').toggleClass("show")
        $('.navigation .back-drop').toggleClass("show")
    });

    $('.navigation .back-drop').on("click", function () {
        $('.navigation .hamburger').toggleClass("is-active")
        $('.navigation .side-drawer').toggleClass("show")
        $('.navigation .back-drop').toggleClass("show")
    });

    $('.navigation-bar__list-item.have-nested-list').on("click", function () {
        $(this).toggleClass("is-active")
    });

    $('.latest-work .latest-work-list').on('click', 'li article', function () {
        const $src = $(this).find('.latest-work-article__image').attr('src');
        const $desc = $(this).find('.latest-work-article__desc').html();

        const $modal = $('.latest-work .picture-modal');

        $modal.find('.picture-modal__picture').attr('src', $src);
        $modal.find('.picture-modal__desc').html($desc);

        $('.latest-work .back-drop, .latest-work .picture-modal__close').addClass('show');
        $modal.addClass('show');
    });

    $('.latest-work .back-drop').on('click', function () {
        $('.latest-work .back-drop').removeClass('show');
        $('.latest-work .picture-modal').removeClass('show');
        $('.latest-work .picture-modal__close').removeClass('show');
    });

    $('.latest-work .picture-modal__close').on('click', function () {
        $('.latest-work .back-drop').removeClass('show');
        $('.latest-work .picture-modal').removeClass('show');
        $('.latest-work .picture-modal__close').removeClass('show');
    });

    $('.latest-work .picture-modal__close').on('click', function () {
        $('.latest-work .latest-work-list li article').toggleClass('show-big');
    });


    $('.latest-work .load-more-button').on("click", latestWorkPaginator);

    function latestWorkPaginator() {

        const $list = $('.latest-work-list');
        const liCount = $list.find('li').length;

        getAjaxLatestWork(6, liCount);
    }

    function getAjaxLatestWork(count, offset) {

        const $list = $('.latest-work-list');
        let listItems;

        $.ajax({
            url: `https://the-modernist-1db78.firebaseio.com/latest-works.json?orderBy="$key"&startAt="${offset + 1}"&endAt="${offset + count + 1}"`,
            dataType: "json",
            method: 'GET',
        })
            .done(res => {
                const response = Object.keys(res).map(e => res[e])
                const showAjaxButton = response[count] ? true : false;

                const spliceResponse = [...response];
                spliceResponse.splice(count ,1);

                listItems = spliceResponse.map((item) => {
                    return `
                        <li class="latest-work-list__item">
                             <article class="latest-work-article">
                                 <img class="latest-work-article__image" src="dist/img/290x190_${item.img_src}.jpg" alt="Work item">
                                 <p class="latest-work-article__desc">${item.desc}</p>
                             </article>
                          </li>`;
                });
                $list.append(listItems);

                if (!showAjaxButton) {
                    $('.latest-work .load-more-button').hide();
                }
            });
    }

    var bg = $('body');

    function resizeBackground() {
        bg.height($(window).height());
    }

    $(window).resize(resizeBackground);
    resizeBackground();
});

